// Code generated by protoc-gen-go. DO NOT EDIT.
// source: repository-service.proto

package gitaly

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type RepositoryExistsRequest struct {
	Repository *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
}

func (m *RepositoryExistsRequest) Reset()                    { *m = RepositoryExistsRequest{} }
func (m *RepositoryExistsRequest) String() string            { return proto.CompactTextString(m) }
func (*RepositoryExistsRequest) ProtoMessage()               {}
func (*RepositoryExistsRequest) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{0} }

func (m *RepositoryExistsRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

type RepositoryExistsResponse struct {
	Exists bool `protobuf:"varint,1,opt,name=exists" json:"exists,omitempty"`
}

func (m *RepositoryExistsResponse) Reset()                    { *m = RepositoryExistsResponse{} }
func (m *RepositoryExistsResponse) String() string            { return proto.CompactTextString(m) }
func (*RepositoryExistsResponse) ProtoMessage()               {}
func (*RepositoryExistsResponse) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{1} }

func (m *RepositoryExistsResponse) GetExists() bool {
	if m != nil {
		return m.Exists
	}
	return false
}

type RepackIncrementalRequest struct {
	Repository *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
}

func (m *RepackIncrementalRequest) Reset()                    { *m = RepackIncrementalRequest{} }
func (m *RepackIncrementalRequest) String() string            { return proto.CompactTextString(m) }
func (*RepackIncrementalRequest) ProtoMessage()               {}
func (*RepackIncrementalRequest) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{2} }

func (m *RepackIncrementalRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

type RepackIncrementalResponse struct {
}

func (m *RepackIncrementalResponse) Reset()                    { *m = RepackIncrementalResponse{} }
func (m *RepackIncrementalResponse) String() string            { return proto.CompactTextString(m) }
func (*RepackIncrementalResponse) ProtoMessage()               {}
func (*RepackIncrementalResponse) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{3} }

type RepackFullRequest struct {
	Repository   *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	CreateBitmap bool        `protobuf:"varint,2,opt,name=create_bitmap,json=createBitmap" json:"create_bitmap,omitempty"`
}

func (m *RepackFullRequest) Reset()                    { *m = RepackFullRequest{} }
func (m *RepackFullRequest) String() string            { return proto.CompactTextString(m) }
func (*RepackFullRequest) ProtoMessage()               {}
func (*RepackFullRequest) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{4} }

func (m *RepackFullRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *RepackFullRequest) GetCreateBitmap() bool {
	if m != nil {
		return m.CreateBitmap
	}
	return false
}

type RepackFullResponse struct {
}

func (m *RepackFullResponse) Reset()                    { *m = RepackFullResponse{} }
func (m *RepackFullResponse) String() string            { return proto.CompactTextString(m) }
func (*RepackFullResponse) ProtoMessage()               {}
func (*RepackFullResponse) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{5} }

type GarbageCollectRequest struct {
	Repository   *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	CreateBitmap bool        `protobuf:"varint,2,opt,name=create_bitmap,json=createBitmap" json:"create_bitmap,omitempty"`
}

func (m *GarbageCollectRequest) Reset()                    { *m = GarbageCollectRequest{} }
func (m *GarbageCollectRequest) String() string            { return proto.CompactTextString(m) }
func (*GarbageCollectRequest) ProtoMessage()               {}
func (*GarbageCollectRequest) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{6} }

func (m *GarbageCollectRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *GarbageCollectRequest) GetCreateBitmap() bool {
	if m != nil {
		return m.CreateBitmap
	}
	return false
}

type GarbageCollectResponse struct {
}

func (m *GarbageCollectResponse) Reset()                    { *m = GarbageCollectResponse{} }
func (m *GarbageCollectResponse) String() string            { return proto.CompactTextString(m) }
func (*GarbageCollectResponse) ProtoMessage()               {}
func (*GarbageCollectResponse) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{7} }

type RepositorySizeRequest struct {
	Repository *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
}

func (m *RepositorySizeRequest) Reset()                    { *m = RepositorySizeRequest{} }
func (m *RepositorySizeRequest) String() string            { return proto.CompactTextString(m) }
func (*RepositorySizeRequest) ProtoMessage()               {}
func (*RepositorySizeRequest) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{8} }

func (m *RepositorySizeRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

type RepositorySizeResponse struct {
	// Repository size in kilobytes
	Size int64 `protobuf:"varint,1,opt,name=size" json:"size,omitempty"`
}

func (m *RepositorySizeResponse) Reset()                    { *m = RepositorySizeResponse{} }
func (m *RepositorySizeResponse) String() string            { return proto.CompactTextString(m) }
func (*RepositorySizeResponse) ProtoMessage()               {}
func (*RepositorySizeResponse) Descriptor() ([]byte, []int) { return fileDescriptor6, []int{9} }

func (m *RepositorySizeResponse) GetSize() int64 {
	if m != nil {
		return m.Size
	}
	return 0
}

func init() {
	proto.RegisterType((*RepositoryExistsRequest)(nil), "gitaly.RepositoryExistsRequest")
	proto.RegisterType((*RepositoryExistsResponse)(nil), "gitaly.RepositoryExistsResponse")
	proto.RegisterType((*RepackIncrementalRequest)(nil), "gitaly.RepackIncrementalRequest")
	proto.RegisterType((*RepackIncrementalResponse)(nil), "gitaly.RepackIncrementalResponse")
	proto.RegisterType((*RepackFullRequest)(nil), "gitaly.RepackFullRequest")
	proto.RegisterType((*RepackFullResponse)(nil), "gitaly.RepackFullResponse")
	proto.RegisterType((*GarbageCollectRequest)(nil), "gitaly.GarbageCollectRequest")
	proto.RegisterType((*GarbageCollectResponse)(nil), "gitaly.GarbageCollectResponse")
	proto.RegisterType((*RepositorySizeRequest)(nil), "gitaly.RepositorySizeRequest")
	proto.RegisterType((*RepositorySizeResponse)(nil), "gitaly.RepositorySizeResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for RepositoryService service

type RepositoryServiceClient interface {
	RepositoryExists(ctx context.Context, in *RepositoryExistsRequest, opts ...grpc.CallOption) (*RepositoryExistsResponse, error)
	RepackIncremental(ctx context.Context, in *RepackIncrementalRequest, opts ...grpc.CallOption) (*RepackIncrementalResponse, error)
	RepackFull(ctx context.Context, in *RepackFullRequest, opts ...grpc.CallOption) (*RepackFullResponse, error)
	GarbageCollect(ctx context.Context, in *GarbageCollectRequest, opts ...grpc.CallOption) (*GarbageCollectResponse, error)
	RepositorySize(ctx context.Context, in *RepositorySizeRequest, opts ...grpc.CallOption) (*RepositorySizeResponse, error)
	// Deprecated, use the RepositoryExists RPC instead.
	Exists(ctx context.Context, in *RepositoryExistsRequest, opts ...grpc.CallOption) (*RepositoryExistsResponse, error)
}

type repositoryServiceClient struct {
	cc *grpc.ClientConn
}

func NewRepositoryServiceClient(cc *grpc.ClientConn) RepositoryServiceClient {
	return &repositoryServiceClient{cc}
}

func (c *repositoryServiceClient) RepositoryExists(ctx context.Context, in *RepositoryExistsRequest, opts ...grpc.CallOption) (*RepositoryExistsResponse, error) {
	out := new(RepositoryExistsResponse)
	err := grpc.Invoke(ctx, "/gitaly.RepositoryService/RepositoryExists", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *repositoryServiceClient) RepackIncremental(ctx context.Context, in *RepackIncrementalRequest, opts ...grpc.CallOption) (*RepackIncrementalResponse, error) {
	out := new(RepackIncrementalResponse)
	err := grpc.Invoke(ctx, "/gitaly.RepositoryService/RepackIncremental", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *repositoryServiceClient) RepackFull(ctx context.Context, in *RepackFullRequest, opts ...grpc.CallOption) (*RepackFullResponse, error) {
	out := new(RepackFullResponse)
	err := grpc.Invoke(ctx, "/gitaly.RepositoryService/RepackFull", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *repositoryServiceClient) GarbageCollect(ctx context.Context, in *GarbageCollectRequest, opts ...grpc.CallOption) (*GarbageCollectResponse, error) {
	out := new(GarbageCollectResponse)
	err := grpc.Invoke(ctx, "/gitaly.RepositoryService/GarbageCollect", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *repositoryServiceClient) RepositorySize(ctx context.Context, in *RepositorySizeRequest, opts ...grpc.CallOption) (*RepositorySizeResponse, error) {
	out := new(RepositorySizeResponse)
	err := grpc.Invoke(ctx, "/gitaly.RepositoryService/RepositorySize", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *repositoryServiceClient) Exists(ctx context.Context, in *RepositoryExistsRequest, opts ...grpc.CallOption) (*RepositoryExistsResponse, error) {
	out := new(RepositoryExistsResponse)
	err := grpc.Invoke(ctx, "/gitaly.RepositoryService/Exists", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for RepositoryService service

type RepositoryServiceServer interface {
	RepositoryExists(context.Context, *RepositoryExistsRequest) (*RepositoryExistsResponse, error)
	RepackIncremental(context.Context, *RepackIncrementalRequest) (*RepackIncrementalResponse, error)
	RepackFull(context.Context, *RepackFullRequest) (*RepackFullResponse, error)
	GarbageCollect(context.Context, *GarbageCollectRequest) (*GarbageCollectResponse, error)
	RepositorySize(context.Context, *RepositorySizeRequest) (*RepositorySizeResponse, error)
	// Deprecated, use the RepositoryExists RPC instead.
	Exists(context.Context, *RepositoryExistsRequest) (*RepositoryExistsResponse, error)
}

func RegisterRepositoryServiceServer(s *grpc.Server, srv RepositoryServiceServer) {
	s.RegisterService(&_RepositoryService_serviceDesc, srv)
}

func _RepositoryService_RepositoryExists_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RepositoryExistsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RepositoryServiceServer).RepositoryExists(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/gitaly.RepositoryService/RepositoryExists",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RepositoryServiceServer).RepositoryExists(ctx, req.(*RepositoryExistsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RepositoryService_RepackIncremental_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RepackIncrementalRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RepositoryServiceServer).RepackIncremental(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/gitaly.RepositoryService/RepackIncremental",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RepositoryServiceServer).RepackIncremental(ctx, req.(*RepackIncrementalRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RepositoryService_RepackFull_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RepackFullRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RepositoryServiceServer).RepackFull(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/gitaly.RepositoryService/RepackFull",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RepositoryServiceServer).RepackFull(ctx, req.(*RepackFullRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RepositoryService_GarbageCollect_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GarbageCollectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RepositoryServiceServer).GarbageCollect(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/gitaly.RepositoryService/GarbageCollect",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RepositoryServiceServer).GarbageCollect(ctx, req.(*GarbageCollectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RepositoryService_RepositorySize_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RepositorySizeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RepositoryServiceServer).RepositorySize(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/gitaly.RepositoryService/RepositorySize",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RepositoryServiceServer).RepositorySize(ctx, req.(*RepositorySizeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RepositoryService_Exists_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RepositoryExistsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RepositoryServiceServer).Exists(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/gitaly.RepositoryService/Exists",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RepositoryServiceServer).Exists(ctx, req.(*RepositoryExistsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _RepositoryService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "gitaly.RepositoryService",
	HandlerType: (*RepositoryServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "RepositoryExists",
			Handler:    _RepositoryService_RepositoryExists_Handler,
		},
		{
			MethodName: "RepackIncremental",
			Handler:    _RepositoryService_RepackIncremental_Handler,
		},
		{
			MethodName: "RepackFull",
			Handler:    _RepositoryService_RepackFull_Handler,
		},
		{
			MethodName: "GarbageCollect",
			Handler:    _RepositoryService_GarbageCollect_Handler,
		},
		{
			MethodName: "RepositorySize",
			Handler:    _RepositoryService_RepositorySize_Handler,
		},
		{
			MethodName: "Exists",
			Handler:    _RepositoryService_Exists_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "repository-service.proto",
}

func init() { proto.RegisterFile("repository-service.proto", fileDescriptor6) }

var fileDescriptor6 = []byte{
	// 370 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x94, 0x41, 0x4f, 0xfa, 0x40,
	0x10, 0xc5, 0xe1, 0xff, 0x27, 0x8d, 0x19, 0xd1, 0xe8, 0x44, 0xb0, 0xd4, 0xa8, 0x58, 0x2f, 0x1e,
	0x94, 0x43, 0xfd, 0x06, 0x1a, 0x34, 0xc6, 0x60, 0x62, 0x3d, 0x98, 0x78, 0x31, 0x4b, 0x9d, 0xe0,
	0xc6, 0x42, 0xeb, 0xee, 0x62, 0x84, 0xb3, 0x1f, 0xdc, 0x64, 0x5b, 0x68, 0x0b, 0x8b, 0x97, 0xea,
	0xad, 0x9d, 0x9d, 0xf9, 0xcd, 0xdb, 0xbe, 0x97, 0x82, 0x2d, 0x28, 0x8e, 0x24, 0x57, 0x91, 0x98,
	0x9c, 0x49, 0x12, 0x1f, 0x3c, 0xa0, 0x4e, 0x2c, 0x22, 0x15, 0xa1, 0x35, 0xe0, 0x8a, 0x85, 0x13,
	0xa7, 0x2e, 0x5f, 0x99, 0xa0, 0x97, 0xa4, 0xea, 0xf6, 0x60, 0xd7, 0x9f, 0x4f, 0x74, 0x3f, 0xb9,
	0x54, 0xd2, 0xa7, 0xf7, 0x31, 0x49, 0x85, 0x1e, 0x40, 0x06, 0xb3, 0xab, 0xed, 0xea, 0xc9, 0xba,
	0x87, 0x9d, 0x84, 0xd2, 0xc9, 0x86, 0xfc, 0x5c, 0x97, 0xeb, 0x81, 0xbd, 0x8c, 0x93, 0x71, 0x34,
	0x92, 0x84, 0x4d, 0xb0, 0x48, 0x57, 0x34, 0x6b, 0xcd, 0x4f, 0xdf, 0xdc, 0x3b, 0x3d, 0xc3, 0x82,
	0xb7, 0x9b, 0x51, 0x20, 0x68, 0x48, 0x23, 0xc5, 0xc2, 0x32, 0x1a, 0xf6, 0xa0, 0x65, 0xe0, 0x25,
	0x22, 0xdc, 0x10, 0xb6, 0x93, 0xc3, 0xab, 0x71, 0x58, 0x66, 0x0b, 0x1e, 0xc3, 0x46, 0x20, 0x88,
	0x29, 0x7a, 0xee, 0x73, 0x35, 0x64, 0xb1, 0xfd, 0x4f, 0x5f, 0xaa, 0x9e, 0x14, 0x2f, 0x74, 0xcd,
	0xdd, 0x01, 0xcc, 0x6f, 0x4b, 0x35, 0xc4, 0xd0, 0xb8, 0x66, 0xa2, 0xcf, 0x06, 0x74, 0x19, 0x85,
	0x21, 0x05, 0xea, 0xcf, 0x75, 0xd8, 0xd0, 0x5c, 0xdc, 0x98, 0x6a, 0xb9, 0x85, 0x46, 0x06, 0x7e,
	0xe0, 0x53, 0x2a, 0xf3, 0xe5, 0x4f, 0xa1, 0xb9, 0x08, 0x4b, 0xbd, 0x47, 0xa8, 0x49, 0x3e, 0x25,
	0xcd, 0xf9, 0xef, 0xeb, 0x67, 0xef, 0xab, 0xa6, 0xbd, 0x98, 0xb5, 0x27, 0x61, 0xc5, 0x47, 0xd8,
	0x5a, 0x4c, 0x10, 0x1e, 0x2e, 0xef, 0x2d, 0x44, 0xd5, 0x69, 0xaf, 0x6e, 0x48, 0xef, 0x59, 0xc1,
	0xa7, 0x99, 0xf3, 0xb9, 0x58, 0x60, 0x7e, 0xd0, 0x98, 0x40, 0xe7, 0xe8, 0x87, 0x8e, 0x39, 0xbb,
	0x0b, 0x90, 0xf9, 0x8c, 0xad, 0xe2, 0x48, 0x2e, 0x69, 0x8e, 0x63, 0x3a, 0x9a, 0x63, 0xee, 0x61,
	0xb3, 0x68, 0x13, 0xee, 0xcf, 0xfa, 0x8d, 0x81, 0x71, 0x0e, 0x56, 0x1d, 0xe7, 0x91, 0x45, 0x4b,
	0x32, 0xa4, 0xd1, 0xf7, 0x0c, 0x69, 0x76, 0xd2, 0xad, 0x60, 0x0f, 0xac, 0x5f, 0xf4, 0xa5, 0x6f,
	0xe9, 0x1f, 0xd1, 0xf9, 0x77, 0x00, 0x00, 0x00, 0xff, 0xff, 0x32, 0x7f, 0x2a, 0xc4, 0xba, 0x04,
	0x00, 0x00,
}
